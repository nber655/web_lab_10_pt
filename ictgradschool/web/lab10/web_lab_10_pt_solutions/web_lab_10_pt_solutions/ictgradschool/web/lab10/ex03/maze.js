"use strict";

// TODO Your code here.

$(function() {

    var person = $("#figure");
    var monster = $("#monster");

    playAnimation(person, 0, 0);
    playAnimation(monster, 1000, -16);

});

function playAnimation(character, delay, offs) {

    if (delay > 0) {
        character.delay(delay);
    }

    character.animate({left: 40 + offs}, 500, "swing")
             .animate({top: "-=65"}, 500, "swing")
             .animate({left: "+=60"}, 500, "swing")
             .animate({top: "+=125"}, 500, "swing")
             .animate({left: "-=60"}, 500, "swing")
             .animate({top: "+=65"}, 500, "swing")
             .animate({left: "+=120"}, 500, "swing")
             .animate({top: "-=125"}, 500, "swing")
             .animate({left: "+=60"}, 500, "swing")
             .animate({top: "+=125"}, 500, "swing")
             .animate({left: "+=60"}, 500, "swing")

}