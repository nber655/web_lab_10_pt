"use strict";

$(document).ready(function () {
	console.log("Initialising controls...");

	/* ************** SETTING UP THE UI WITH jQuery UI ***************/

	$("#choose-background").buttonset();
	$("#toggle-components").buttonset();

	var fullheight = $("#container").height();
	var medianLine = fullheight / 2;

	var scalePropertyValue = "scale(1.0)";

	$("#size-control").slider({ // dolphins start at scale 1.0 and can be scaled between 25% and 200% size (0.25 and 2.0 times the size)
		min: 25,
		max: 200,
		value: 100
	});

	/* ************** HANDLING EVENTS FROM SLIDER CONTROL ***************/

	$("#size-control").on("slide", function (event, ui) { // every time the slider moves

		// ui.value is out of 100, i.e. percent
		// but our dolphins already start at 50% (or currsize %) of their max-size. So resize them with respect to that

		// When the slider is at the half-way mark (ui.value is 50), the image should be at 100% size, so twice the ui.value
		// When the slider is at max/100% slider position, that means scaling the image to 200% (2.0 times)
		// When the slider is less than 50%, we're shrinking it (less than 100% the original size).
		// The slider value ranges from 0 to 100 by default, so we multiply the slider value by 2 and divide by 100 to
		// get the decimal fraction we want to scale the image by

		console.log(ui.value);

		var sliderValue = ui.value / 100; // get decimal version of scale value
		scalePropertyValue = "scale(" + sliderValue + "," + sliderValue + ")"; // the value of the transform CSS property: scale image

		$(".dolphin").css("transform", scalePropertyValue);

	});

	/* *****************EVENT HANDLING: PLAIN jQuery FROM HERE ON **************/
	/* ************** HANDLING RADIO AND CHECKBOX CONTROLS ***************/

	// https://api.jquery.com/change/
	// http://stackoverflow.com/questions/8908943/get-the-currently-selected-radio-button-in-a-jquery-ui-buttonset-without-binding
	// http://stackoverflow.com/questions/2731794/jquery-radio-button-when-checked-need-an-alert

	$("#choose-background").change(function () {

		// get the chosen radio button
		var id = $("#choose-background input:checked").attr("id");
		var imgname = id.substring("radio-".length);
		imgname += (imgname === "background6") ? ".gif" : ".jpg";

		// change the background image accordingly
		$("#background").attr("src", "../images/ex04/" + imgname);

	}).change(); // call change() the very first time to make the choose-background handler process whatever radio is selected on page load


	$("#toggle-components").change(function () {

		// for every <input> field in toggle-component:
		// if checked, show its associated dolphin image 
		// if unchecked, hide its associated dolphin image 
		$("#toggle-components input").each(function () {
			var id = this.id;
			id = id.substring("check-".length);
			if ($(this).is(":checked")) {
				$("#" + id).show();
			} else {
				$("#" + id).hide();
			}
		});

	}).change(); // call change() the very first time to make the toggle-component handler process whatever is checked on page load

}); // end $(document).ready()