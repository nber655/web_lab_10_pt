"use strict";

// TODO Your code here.

function makePageInteractive() {
    // Initialise page     
    $("img.dolphin").hide();
    $("input:radio").checkboxradio();
    $("input:checkbox").checkboxradio();
    $("#size-control").slider();
    initialBackground();

    // var text = 0;
    // $("#choose-background input:radio").click(function() {
        
    //     $("input:radio").each(function (index) {
            
            
    //         console.log((text + 1));
    //         var srcPath = "../images/ex04/background" + "3" + ".jpg";
    //         // alert(srcPath);
    //         $("#background").attr("src", srcPath);
    //         text++;
    //         return false;
            
    //     });
    // });
   

    // Change background
    $("input:radio[id=radio-background1]").click(function () {
        $("#background").attr("src", "../images/ex04/background1.jpg");
    });

    $("input:radio[id=radio-background2]").click(function () {
        $("#background").attr("src", "../images/ex04/background2.jpg");
    });

    $("input:radio[id=radio-background3]").click(function () {
        $("#background").attr("src", "../images/ex04/background3.jpg");
    });

    $("input:radio[id=radio-background4]").click(function () {
        $("#background").attr("src", "../images/ex04/background4.jpg");
    });

    $("input:radio[id=radio-background5]").click(function () {
        $("#background").attr("src", "../images/ex04/background5.jpg");
    });

    $("input:radio[id=radio-background6]").click(function () {
        $("#background").attr("src", "../images/ex04/background6.gif");
    });

    //Change image
    $("input:checkbox[id=check-dolphin1]").click(function () {
        $("#dolphin1").toggle();
    });

    $("input:checkbox[id=check-dolphin8]").click(function () {
        $("#dolphin8").toggle();
    });

    $("input:checkbox[id=check-dolphin5]").click(function () {
        $("#dolphin5").toggle();
    });

    $("input:checkbox[id=check-dolphin3]").click(function () {
        $("#dolphin3").toggle();
    });

    $("input:checkbox[id=check-dolphin6]").click(function () {
        $("#dolphin6").toggle();
    });

    $("input:checkbox[id=check-dolphin4]").click(function () {
        $("#dolphin4").toggle();
    });

    $("input:checkbox[id=check-dolphin2]").click(function () {
        $("#dolphin2").toggle();
    });
    $("input:checkbox[id=check-dolphin7]").click(function () {
        $("#dolphin7").toggle();
    });

}


function initialBackground() {
    // Select initial background
    // Selecting all <input> tags type=radio.
    var $chooseBackground = $("#choose-background input:radio");
    // Determine which radio button is check 
    for (var i = 0; i < $chooseBackground.length; i++) {

        var $checked = $chooseBackground.eq(i).attr("checked");
        if ($checked) {

            var fileName = "../images/ex04/background" + (i + 1) + ".jpg";
            $("#background").attr("src", fileName);

        }
    }

    // Select initial image
    // Selecting all <input> tags type=radio.
    var $initialImage = $("#toggle-components input:checkbox");
    // Determine which radio button is check 
    for (var z = 0; z < $initialImage.length; z++) {

        var $checked1 = $initialImage.eq(z).attr("checked");
        if ($checked1) {

            var text1 = "#" + "dolphin" + (z + 1);
            $(text1).show();

        }
    }
}



// Call funtion on page load - equivalent to window.onload = functionName;
$(makePageInteractive);

//*************************************************************************


    // // Change background when radio button checked
    // $("input:radio[id=radio-background1]").click(function () {
    //     $("#background").attr("src", "../images/ex04/background1.jpg");
    // });

    // $("input:radio[id=radio-background2]").click(function () {
    //     $("#background").attr("src", "../images/ex04/background2.jpg");
    // });

    // $("input:radio[id=radio-background3]").click(function () {
    //     $("#background").attr("src", "../images/ex04/background3.jpg");
    // });

    // $("input:radio[id=radio-background4]").click(function () {
    //     $("#background").attr("src", "../images/ex04/background4.jpg");
    // });

    // $("input:radio[id=radio-background5]").click(function () {
    //     $("#background").attr("src", "../images/ex04/background5.jpg");
    // });

    // $("input:radio[id=radio-background6]").click(function () {
    //     $("#background").attr("src", "../images/ex04/background6.gif");
    // });

    // Change image 

//**********************************************************************************

// function changeBackground() {

//     $( "#background" ).attr("src", "../images/ex04/background2.jpg");

// }

//***********************************************************************************

//     var $backgroundImage = $( "#choose-background input:[type='radio']" );

// $( "img.dolphin" ).hide();
// // Selecting all <input> tags type=radio.
// // var $chooseBackground = $("#choose-background").find("input:radio");
// var $chooseBackground = $( "#choose-background input:radio");
// // Viewing the number of <input> tags on the page.
// // alert( $chooseBackground.length );

// for (var i=0; i<$chooseBackground.length; i++) {
//     // alert("Child " + i + ": " + $chooseBackground[i]);

//     var $checked = $chooseBackground.eq( i ).attr("checked");
//     if ($checked) {
//         var text = "#" + "dolphin" + (i+1);
//         // alert( "Botton selected is: " + text ); 
//         $(text).show();
//         var fileName = "../images/ex04/background" + (i+1) + ".jpg";

//         // $( "#background" ).attr("src", "../images/ex04/background5.jpg");
//         $( "#background" ).attr("src", fileName);
//     } 
// }

//*********************************************************************************
// Make all paragraph bold when button clicked
// $("#makeBold").click(function() {
//      $("p").css("font-weight", "bold")
// });

// $( function() {
//     $( "input" ).checkboxradio("refresh" );
// } );

// var backgroundImage = $( "#background" );

// // .attr() as a setter: Set attribute src for HTML element with Id "background"
// $( "#background" ).attr("src", "../images/ex04/background1.jpg");

// // .attr() as a getter: Get attribute src for HTML element with Id "background"
// $( "#background" ).attr( "src" );

// // Find all Radio Buttons
// var radioInputs = $( "input:[type='radio']" );