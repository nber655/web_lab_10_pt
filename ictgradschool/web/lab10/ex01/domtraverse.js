"use strict";

// var bodyElement = document.body;
// var divs = bodyElement.getElementsByTagName("div");
// divs[0].style.backgroundColor = "pink";

// Instead of selecting all the divs, use a specific class of the divs
// Change the background of div with class "text1"
var getDivs = $(".text1");
getDivs.css("background-color", "pink");

$(".text1 i").css("background-color", "green");
$(".text1 p").css("font-weight", "bold");

$(document).ready(function () {
    $(".myclass").click(function () {
        $(this).hide();
    });
});
/* 
 //One way to get the last p and make it red:
 var paragraphs = bodyElement.getElementsByTagName("p");
 lastParagraph = paragraphs[paragraphs.length-1];
 lastParagraph.style.color="red";
 */

//Another way, better:
// var lastParagraph = document.getElementById("footer"); //Note, getElementById is a method of the document node, not a method of any element nodes!!!
// lastParagraph.style.color = "red";

// change the color of text paragraph with id "footer"
var getFooter = $("#footer");
getFooter.css("color", "red");


// divs[1].style.visibility = "hidden";
// hide div with class "text2"
$(".text2").css("visibility", "hidden");
/*
 // Another way. But unlike setting visibility, this way does not preserve the space allocated to the div (the 'block' display of divs):
 divs[1].style.display="none";
 */

// One way:
// we get back a list of all elements with class=subtitle, even though the list returned only happens to have one item this time.
// var subtitles = bodyElement.getElementsByClassName("subtitle");
// get the first subtitle, get its first child (<small>), get its first child which is a textnode and then the latter's value
// subtitles[0].firstChild.nodeValue = "Hello World";

// Change the text inside <small> eement within class "subtitle"
$(".subtitle small").text("Hello World");


// Make all paragraph bold when button clicked
$("#makeBold").click(function () {
    $("p").css("font-weight", "bold")
});

// var myButton = document.getElementById("makeBold");

//var myButtons = bodyElement.getElementsByTagName("button");
//console.log(myButtons[0]);

// When you click the button, set the style for all paragraphs. Accomplished without looping,
// uses createElement() to create a <style> element. See http://www.w3schools.com/jsref/dom_obj_style.asp
// myButton.addEventListener("click", function () {
//     var styleElement = document.createElement("STYLE");
//     styleElement.innerHTML = "p {font-weight: bold}";
//     document.head.appendChild(styleElement);
// });

