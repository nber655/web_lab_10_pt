"use strict";

// TODO Your code here.


function mazeAnimation() {
    // $("#wrapper").mouseover(function () {
        // Animate figure
        var figure = $("#figure");
        figure.animate({ left: '35px' }, "slow", "linear");
        figure.animate({ top: '35px' }, "slow", "linear");
        figure.animate({ left: '95px' }, "slow", "linear");
        figure.animate({ top: '160px' }, "slow", "linear");
        figure.animate({ left: '35px' }, "slow", "linear");
        figure.animate({ top: '225px' }, "slow", "linear");
        figure.animate({ left: '160px' }, "slow", "linear");
        figure.animate({ top: '100px' }, "slow", "linear");
        figure.animate({ left: '220px' }, "slow", "linear");
        figure.animate({ top: '225px' }, "slow", "linear");
        figure.animate({ left: '240px' }, "slow", "linear");
        figure.animate({ left: '350px' }, "slow", "linear");

        // Animate monster

        var monster = $("#monster");
        monster.delay(1200).animate({ left: '20px' }, "slow", "linear");
        monster.animate({ top: '35px' }, "slow", "linear");
        monster.animate({ left: '80px' }, "slow", "linear");
        monster.animate({ top: '160px' }, "slow", "linear");
        monster.animate({ left: '20px' }, "slow", "linear");
        monster.animate({ top: '225px' }, "slow", "linear");
        monster.animate({ left: '140px' }, "slow", "linear");
        monster.animate({ top: '100px' }, "slow", "linear");
        monster.animate({ left: '200px' }, "slow", "linear");
        monster.animate({ top: '225px' }, "slow", "linear");
        monster.animate({ left: '235px' }, "slow", "linear");
        monster.animate({ left: '300px' }, "slow", "linear");

    // });
}
// Call function when loading the page
$(mazeAnimation);

