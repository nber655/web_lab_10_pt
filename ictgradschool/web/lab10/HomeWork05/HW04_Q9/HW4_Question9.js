"use strict";


// HomeWork 4 - Question 9

// The following array has been defined:

// var cars = [
//   {year: 2007, make: "BMW", model: "323i"},
//   {year: 2005, make: "Renault", model: "Megane"},
//   {year: 2008, make: "Jaguar", model: "XF 3.0"}
// ];
// Write JavaScript code which will create an HTML Table in which to display this data, 
// and add the table to the document.body. The table should have three columns: one each for the 
// cars' make, model and year. All three cars should also be displayed in the result table. You don't need to worry about column headings.


  // Create Car array
  var cars = [
    {year: 2007, make: "BMW", model: "323i"},
    {year: 2005, make: "Renault", model: "Megane"},
    {year: 2008, make: "Jaguar", model: "XF 3.0"}
  ]

function createmyTable() {

    // Check if table already exists. If it does, display an error and quit the function.
    // var mytable = document.getElementById("Table1");
    // if (mytable != null) {
    //     alert("Table already exists!")
    //     return;
    // }



    // Create table and add properties
    var mytable = document.createElement("table");
    mytable.id = "Table1";
    mytable.classList.add("table");
    // mytable.classList.add("table-striped");

    // Create table header
    var mytable_header = document.createElement("thead");
    var mytable_header_row = document.createElement("tr");
    // Create header rows
    // var header_cell1 = document.createElement("th");
    // header_cell1.innerHTML = "Year";

    // var header_cell2 = document.createElement("th");
    // header_cell2.innerHTML = "Make";

    // var header_cell3 = document.createElement("th");
    // header_cell3.innerHTML = "Model";

    // // Add cells to row
    // mytable_header_row.appendChild(header_cell1);
    // mytable_header_row.appendChild(header_cell2);
    // mytable_header_row.appendChild(header_cell3);

for(var i = 0; i < 1; i++) {
  for(var key in cars[i]) {
    var header_cell = document.createElement("th");
    header_cell.innerHTML = key;
    mytable_header_row.appendChild(header_cell);
  }
}
    // Add row to table header
    mytable_header.appendChild(mytable_header_row);

    // Create a table body, rows and populate each row cell with array data
    var mytable_body = document.createElement("tbody");
    
    for(var cars_array_index in cars) {
      var mytable_body_row = document.createElement("tr");
      // var row_cell1 = document.createElement("td");
      // row_cell1.innerHTML = cars[property].year;
      // var row_cell2 = document.createElement("td");
      // row_cell2.innerHTML = cars[property].make;
      // var row_cell3 = document.createElement("td");
      // row_cell3.innerHTML = cars[property].model;

      // row.appendChild(row_cell1);
      // row.appendChild(row_cell2);
      // row.appendChild(row_cell3);
      for(var cars_object_key in cars[cars_array_index]) {
        var body_cell = document.createElement("td");
        body_cell.innerHTML = cars[cars_array_index][cars_object_key];
        mytable_body_row.appendChild(body_cell);
      }
      
      mytable_body.appendChild(mytable_body_row);
    }

    // Add table header and body to table
    mytable.appendChild(mytable_header);
    mytable.appendChild(mytable_body);

    // Add table to div in HTML document
    var div_container = document.getElementsByClassName("mytableContainer");
    div_container[0].appendChild(mytable);

}

// function addRow() {

//     // Get the table body
//     var tbody = document.getElementById("Table1Body");

//     // if it doesn't exist, get outta here.
//     if (tbody == null) {
//         alert("Table doesn't exist. Create it first.");
//         return;
//     }

//     // How many rows are there already?
//     var count = tbody.childNodes.length;

//     // Create some HTML. This is another way we can add elements to the page.
//     var rowHTML = "<tr><td>Row " + count + ", column 1</td><td>Row " + count + ", column 2</td></tr>";
//     tbody.innerHTML += rowHTML;

// }

