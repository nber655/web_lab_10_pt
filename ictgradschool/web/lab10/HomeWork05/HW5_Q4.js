"use strict";

// HomeWork 5 - Question 4

// The following JavaScript array has been defined:

// var cars = [
//   {year: 2005, make: "Nissan", model: "Skyline"},
//   {year: 2004, make: "Honda", model: "Fit"},
//   {year: 2006, make: "Holden", model: "HSV GTS"}
// ];
// Write code which performs the following:

// Creates an HTML table
// Creates a header with the columns "Year", "Make", and "Model"
// Creates one row for each element in the cars array, and fills the table with the data in the array
// Adds the table to the <div> with id = "container"
// Use jQuery wherever possible.

var cars = [
    { year: 2005, make: "Nissan", model: "Skyline" },
    { year: 2004, make: "Honda", model: "Fit" },
    { year: 2006, make: "Holden", model: "HSV GTS" }
];

$(document).ready(function () {
    console.log("ready!");

    // Create table, table header, header row and add class to table
    var $mytable = $("<table id='mytable1'><thead><tr id='mytheadtr'></tr></thead></table>").addClass("table");
    $mytable.appendTo(".container");
    $("#mytable1").append("<tbody id='mytbody'></tbody>");

    // Concatenate all the <th> tags into a single string, 
    // Use an array to gather all the strings <th>s
    // then append the array (with <th>s) to the <tr> tag with id "mytheadtr"
    var headrowCells = [];
    for (var key in cars[0]) {
        headrowCells.push("<th>" + key.toUpperCase() + "</th>");
    }

    $("#mytheadtr").append(headrowCells.join(""));
    var row = [];
    for (var index in cars) {

        var trId = "tbodyRow" + index;
        var rowId = "#" + trId;
        console.log(rowId);

        $("#mytbody").append("<tr id='" + trId + "'></tr>");
        var cell = [];
        for (var objKey in cars[index]) {

            cell.push("<td>" + cars[index][objKey] + "</td>");
        }
        $(rowId).append(cell.join(""));
    }
});






