"use strict";

// HomeWork 5 - Question 6

// Write code which uses jQuery's animate() method to animate #box such that it does the following:

// Changes its width to 90% of the available width over 300ms, THEN
// Changes its font size to 24px over 1000ms, THEN
// Changes its lefthand border width to 15px WHILE also increasing its height to 200px, over 500ms.

// To set a #box DIV as 90% of the full 100% screen width and height. 
// You need to set the width and height on the <html> and <body> elements  to 100% using css
$(document).ready(function () {
    // console.log("ready!");
    // console.log($(window).width());
    // console.log($(window).height());
    // console.log($("div#box").parent().width());
    // console.log($("div#box").width());
    $("#box").animate({width: "90%"}, 300);
    $("#box").animate({fontSize: "24px"}, 1000);
    $("#box").animate({borderLeftWidth: "15px", height: "200px"}, 500);

});