"use script";

// Remember to add the jQuery link to the HTML document to get it working

// Makes the bauble to fall when mouse over each one
function makePageInteractive() {
    // var container = document.getElementById("container");
    // var baubles = container.getElementsByClassName("bauble");
    // var baubles = document.getElementsByClassName("bauble");
    var baubleImg = $(".bauble");
    baubleImg.mouseover(function () {
        $(this).addClass("fall");
    });
    
    // var i = -1;

    // for (i = 0; i < baubles.length; i++) {

    //     baubles[i].onmouseover = function () {

    //         // Add the "fall" class to the bauble.
    //         this.classList.add("fall");
    //     }
    // }
}

// Assign an event handler so that the makePageInteractive function is called when the window has loaded. 
// window.onload = makePageInteractive;

// Loading the page
$(makePageInteractive);